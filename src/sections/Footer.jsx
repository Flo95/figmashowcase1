import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

const StyledFooter = styled.div`
  width: 980px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const StyledFooterRow = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: 60px;
`;

const StyledFooterColumn = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  gap: 20px;
  margin-right: ${({ $marginRight }) =>
    $marginRight ? $marginRight : "unset"};
  width: ${({ $width }) => ($width ? $width : "unset")};

  & h4 {
    color: #fff;
    font-family: Apercu;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  & p {
    color: #fff;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
    cursor: pointer;
  }
`;

const StyledDivider = styled.hr`
  width: 100%;
  height: 1px;
  opacity: 0.1;
  background: #fff;
`;

const StyledLastRow = styled.div`
  width: 100%;
  margin-top: 50px;
  display: flex;
  justify-content: space-between;

  & p {
    color: #fff;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
`;

function Footer() {
  return (
    <SectionWrapper
      padding="120px 230px"
      justifyContent="center"
      backgroundColor="#00063F"
    >
      <StyledFooter>
        <StyledFooterRow>
          <StyledFooterColumn $marginRight="123px">
            <h4>Company</h4>
            <p>About</p>
            <p>Pricing</p>
            <p>Jobs</p>
            <p>Blog</p>
            <p>Careers</p>
          </StyledFooterColumn>
          <StyledFooterColumn $marginRight="54px">
            <h4>Product</h4>
            <p>Sales software</p>
            <p>Features</p>
            <p>Privacy and security</p>
            <p>Marketplace</p>
            <p>API</p>
          </StyledFooterColumn>
          <StyledFooterColumn $marginRight="182px">
            <h4>Help Center</h4>
            <p>Community</p>
            <p>Knowledge Base</p>
            <p>Academy</p>
            <p>Support</p>
          </StyledFooterColumn>
          <StyledFooterColumn $width="280px">
            <h4>Download</h4>
            <p>
              Join millions of people who build a fully integrated sales and
              maketing solution.
            </p>
          </StyledFooterColumn>
        </StyledFooterRow>
        <StyledDivider />
        <StyledLastRow>
          <p>Copyright @2023 Aspire. All Rights Reserved.</p>
          <p>Terms & Conditions ~ Privacy Policy</p>
        </StyledLastRow>
      </StyledFooter>
    </SectionWrapper>
  );
}

export default Footer;
