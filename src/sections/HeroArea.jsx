import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import EmailInput from "../components/EmailInput";
import CheckSVG from "../images/heroArea/Check.svg";

const StyledWrapper = styled.div`
  width: 835px;
  height: 310px;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  flex-shrink: 0;

  & h1 {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 56px;
    font-style: normal;
    font-weight: 700;
    line-height: 60px;
    margin-bottom: 20px;
  }

  & h6 {
    color: #111827;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
    margin-bottom: 50px;
  }
`;

function HeroArea() {
  return (
    <SectionWrapper padding="80px 302px" justifyContent="center">
      <StyledWrapper>
        <h1>Multipurpose Website Templates That boost your sales</h1>
        <h6>
          Empowering Your Financial Journey through Customized Asset Management
          Services
        </h6>
        <EmailInput imgData={{ src: CheckSVG, alt: "check" }} />
      </StyledWrapper>
    </SectionWrapper>
  );
}

export default HeroArea;
