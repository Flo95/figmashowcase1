import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";
import StatisticCard from "../components/StatisticCard";

import Icon1 from "../images/stats/Icon-1.svg";
import Icon2 from "../images/stats/Icon-2.svg";
import Icon3 from "../images/stats/Icon-3.svg";
import Icon4 from "../images/stats/Icon-4.svg";

const StyledStatistics = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 79px;

  & h1 {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 56px;
    font-style: normal;
    font-weight: 700;
    line-height: 60px;
    width: 905px;
  }
`;

const StyledCardRow = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 20px;
`;

function Statistics() {
  return (
    <SectionWrapper padding="120px 130px 20px 130px" justifyContent="center">
      <StyledStatistics>
        <h1>Our 12 years of together have given us much to be proud of</h1>
        <StyledCardRow>
          <StatisticCard
            imgData={{ src: Icon1, alt: "Icon-1" }}
            title="50M+"
            text="Wel illum qui dolorem eum fugiat quo voluptas nulla pariatur"
          />
          <StatisticCard
            imgData={{ src: Icon2, alt: "Icon-2" }}
            title="$100M"
            text="Eis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam"
          />
          <StatisticCard
            imgData={{ src: Icon3, alt: "Icon-3" }}
            title="30%"
            text="Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam"
          />
          <StatisticCard
            imgData={{ src: Icon4, alt: "Icon-4" }}
            title="60+"
            text="Magni dolores eos qui ratione voluptatem sequi nesciunt"
          />
        </StyledCardRow>
      </StyledStatistics>
    </SectionWrapper>
  );
}

export default Statistics;
