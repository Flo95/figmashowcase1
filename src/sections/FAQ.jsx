import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import FAQCard from "../components/FAQCard";

const StyledFAQ = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 60px;
`;

const StyledFAQHeader = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 20px;

  & h1 {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 56px;
    font-style: normal;
    font-weight: 700;
    line-height: 60px; /* 107.143% */
  }

  & h6 {
    color: #111827;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
  }
`;

const StyledFAQWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  width: 980px;
  gap: 20px;
`;

function FAQ() {
  return (
    <SectionWrapper
      padding="120px 230px"
      justifyContent="center"
      backgroundColor="#F6F5F8"
    >
      <StyledFAQ>
        <StyledFAQHeader>
          <h1>Frequently asked questions</h1>
          <h6>We haveput together some commonly asked questions</h6>
        </StyledFAQHeader>

        <StyledFAQWrapper>
          <FAQCard question="Smod tempor incididunt ut labore et dolore" />
          <FAQCard question="Ut enim ad minim veniam" />
          <FAQCard question="Quis nostrud exercitation ullamco laboris" />
          <FAQCard question="Wisi ut aliquip ex ea commodo consequat" />
          <FAQCard question="Woluptate velit esse cillum dolore eu fugiat nulla" />
          <FAQCard question="Duis aute irure dolor in reprehenderi" />
          <FAQCard question="Excepteur sint occaecat cupidatat non proiden" />
          <FAQCard question="Excepteur sint occaecat cupidatat non proiden" />
        </StyledFAQWrapper>
      </StyledFAQ>
    </SectionWrapper>
  );
}

export default FAQ;
