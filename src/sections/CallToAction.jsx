import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import EmailInput from "../components/EmailInput";

import WhiteCheckSVG from "../images/callToAction/WhiteCheck.svg";

const StyledCallToAction = styled.div`
  width: 1180px;
  height: 430px;
  padding: 80px 210px;
  display: flex;
  flex-direction: column;
  align-items: center;
  flex-shrink: 0;
  border-radius: 10px;
  background: #0a85ea;

  & h1 {
    color: #fff;
    text-align: center;
    font-family: Apercu;
    font-size: 56px;
    font-style: normal;
    font-weight: 700;
    line-height: 60px;
    margin-bottom: 20px;
  }

  & p {
    width: 654px;
    color: #fff;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    margin-bottom: 50px;
  }
`;

function CallToAction() {
  return (
    <SectionWrapper padding="120px 130px 25px 130px" justifyContent="center">
      <StyledCallToAction>
        <h1>Get started with Aspire today</h1>
        <p>
          We are self-service data analytics software that lets you create
          visually appealing data visualisations and insightful dashboards in
          minutes.
        </p>
        <EmailInput
          imgData={{ src: WhiteCheckSVG, alt: "check" }}
          hintColor="#FFF"
          hintOpacity={false}
        />
      </StyledCallToAction>
    </SectionWrapper>
  );
}

export default CallToAction;
