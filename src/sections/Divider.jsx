import styled from "styled-components";

const StyledWrapper = styled.div`
  padding: 0px 130px;
  display: flex;
  justify-content: center;
`;

const StyledDivider = styled.hr`
  width: 1080px;
  height: 1px;
  opacity: 0.1;
  color: #000;
`;

function Divider() {
  return (
    <StyledWrapper>
      <StyledDivider />
    </StyledWrapper>
  );
}

export default Divider;
