import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import Screenshot from "../images/productScreenshot/screenshot.png";

const StyledImgWrapper = styled.div`
  width: 857px;
  height: 522px;
  flex-shrink: 0;
  border-radius: 26px 26px 0px 0px;
  padding: 9px 8px 0px 8px;
  background: #000;
`;

function ProductScreenshot() {
  return (
    <SectionWrapper paddiing="0px 291px" justifyContent="center">
      <StyledImgWrapper>
        <img src={Screenshot} alt="ProductScreenshot" />
      </StyledImgWrapper>
    </SectionWrapper>
  );
}

export default ProductScreenshot;
