import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import MeetingPicture from "../images/aboutUs/Meeting.png";
import WhiteArrow from "../images/aboutUs/WhiteArrow.svg";

const StyledAboutUs = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  gap: 101px;
`;

const StyledTextRow = styled.div`
  display: flex;
  justify-content: space-between;
`;

const StyledLeftText = styled.div`
  display: flex;
  flex-direction: column;
  width: 281px;

  & h1 {
    color: #fff;
    font-family: Apercu;
    font-size: 56px;
    font-style: normal;
    font-weight: 700;
    line-height: 60px;
    margin-bottom: 60px;
  }

  & div {
    display: flex;
    gap: 7px;
    cursor: pointer;
  }

  & p {
    color: #fff;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }
`;

const StyledRightText = styled.div`
  width: 577px;

  & p {
    color: #fff;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
  }
`;

function AboutUs() {
  return (
    <SectionWrapper
      padding="120px 230px 124px 230px"
      justifyContent="center"
      backgroundColor="#0A85EA"
    >
      <StyledAboutUs>
        <StyledTextRow>
          <StyledLeftText>
            <h1>About our company</h1>
            <div>
              <p>Learn More</p>
              <img src={WhiteArrow} alt="WhiteArrow" />
            </div>
          </StyledLeftText>
          <StyledRightText>
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
              enim ad minim veniam, quis nostrud exercitation ullamco laboris
              nisi ut aliquip ex ea commodo consequat.
            </p>
            <br />
            <p>
              Duis aute irure dolor in reprehenderit in voluptate velit esse
              cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat
              cupidatat non proident, sunt in culpa qui officia deserunt mollit
              anim id est laborum.
            </p>
            <br />
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo
            </p>
          </StyledRightText>
        </StyledTextRow>
        <img src={MeetingPicture} alt="MeetingPicture" />
      </StyledAboutUs>
    </SectionWrapper>
  );
}

export default AboutUs;
