import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import TeamCard from "../components/TeamCard";

import TeamMember1 from "../images/team/Image-1.png";
import TeamMember2 from "../images/team/Image-2.png";
import TeamMember3 from "../images/team/Image-3.png";
import TeamMember4 from "../images/team/Image-4.png";
import TeamMember5 from "../images/team/Image-5.png";
import TeamMember6 from "../images/team/Image-6.png";
import TeamMember7 from "../images/team/Image-7.png";
import TeamMember8 from "../images/team/Image-8.png";
import RedArrow from "../images/team/RedArrow.svg";

const StyledTeam = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 60px;
`;

const StyledTeamHeader = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  gap: 20px;

  & div {
    display: inline-flex;
    padding: 5px 15px;
    align-items: flex-start;
    gap: 10px;
    border-radius: 60px;
    background: rgba(246, 58, 97, 0.1);

    & p {
      color: #f63a61;
      text-align: center;
      font-family: Apercu;
      font-size: 12px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      text-transform: uppercase;
    }
  }

  & h1 {
    color: #0f0049;
    font-family: Apercu;
    font-size: 56px;
    font-style: normal;
    font-weight: 700;
    line-height: 60px;
  }

  & h6 {
    width: 553px;
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
  }
`;

const StyledTeamRow = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 20px;
`;

const StyledContentWrapper = styled.div`
  width: 1180px;
  height: 217px;
  flex-shrink: 0;
  display: flex;
  justify-content: space-between;
  padding: 60px 108px 60px 100px;
  border-radius: 10px;
  border: 1px solid #eaeaea;
  background: #f6f5f8;

  & h1 {
    color: #0f0049;
    font-family: Apercu;
    font-size: 28px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }
`;

const StyledRightContent = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  gap: 20px;

  & p {
    width: 670px;
    color: #0f0049;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
  }

  & div {
    display: flex;
    align-items: center;
    gap: 5px;
    cursor: pointer;

    & h6 {
      color: #f63a61;
      font-family: Apercu;
      font-size: 16px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
    }

    & img {
      width: 11.069px;
      height: 9.973px;
      flex-shrink: 0;
    }
  }
`;

function Team() {
  function triggerEasterEgg() {
    console.log("Glückwunsch! Du hast das Easter Egg gefunden!");
  }

  return (
    <SectionWrapper padding="120px 130px" justifyContent="center">
      <StyledTeam>
        <StyledTeamHeader>
          <div>
            <p>Our team</p>
          </div>
          <h1>Meet our dream team</h1>
          <h6>
            We're dedicated group of Sed ut perspiciatis unde omnis iste natus
            error sit voluptatem accusantium doloremque laudantium
          </h6>
        </StyledTeamHeader>
        <StyledTeamRow>
          <TeamCard
            imgData={{ src: TeamMember1, alt: "teamMember1" }}
            name="Kristopher A. Davis"
          />
          <TeamCard
            imgData={{ src: TeamMember2, alt: "teamMember2" }}
            name="Maggie W. Roth"
          />
          <TeamCard
            imgData={{ src: TeamMember3, alt: "teamMember3" }}
            name="Anthony N"
          />
          <TeamCard
            imgData={{ src: TeamMember4, alt: "teamMember4" }}
            name="Tony J. Wilcoxen"
          />
        </StyledTeamRow>
        <StyledTeamRow>
          <TeamCard
            imgData={{ src: TeamMember5, alt: "teamMember5" }}
            name="Kristopher A. Davis"
          />
          <TeamCard
            imgData={{ src: TeamMember6, alt: "teamMember6" }}
            name="Maggie W. Roth"
          />
          <TeamCard
            imgData={{ src: TeamMember7, alt: "teamMember7" }}
            name="Anthony N"
          />
          <TeamCard
            imgData={{ src: TeamMember8, alt: "teamMember8" }}
            name="Tony J. Wilcoxen"
          />
        </StyledTeamRow>
        <StyledContentWrapper>
          <h1>Join our team</h1>
          <StyledRightContent>
            <p>
              Sed ut perspiciatis unde omnis iste natus error sit voluptatem
              accusantium doloremque laudantium, totam rem aperiam, eaque ipsa
              quae ab illo inventore veritatis et quasi architecto beatae vitae
              dicta sunt explicabo.
            </p>
            <div onClick={triggerEasterEgg}>
              <h6>View Current Openings</h6>
              <img src={RedArrow} alt="RedArrow" />
            </div>
          </StyledRightContent>
        </StyledContentWrapper>
      </StyledTeam>
    </SectionWrapper>
  );
}

export default Team;
