import styled from "styled-components";

import headerLogo from "../images/header/Logo.png";

const StyledHeader = styled.header`
  padding: 35px 130px 0px 130px;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Divider = styled.hr`
  width: 1180px;
  height: 1px;
  opacity: 0.1;
  color: #000;
`;

const StyledMenuWrapper = styled.div`
  width: 1180px;
  height: 70px;
  display: flex;
  align-items: start;
  justify-content: space-between;
  flex-shrink: 0;

  & p {
    color: #111827;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
    cursor: pointer;
  }
`;

const StyledImg = styled.img`
  width: 111.183px;
  height: 30px;
  margin-top: 3px;
  flex-shrink: 0;
  mix-blend-mode: darken;
`;

const StyledMenu = styled.nav`
  margin-top: 7px;
  display: inline-flex;
  align-items: flex-start;
  gap: 40px;
`;

const StyledContainer = styled.div`
  display: inline-flex;
  align-items: center;
  gap: 19px;

  & button {
    display: inline-flex;
    padding: 8px 15px;
    align-items: flex-start;
    gap: 10px;
    border: 1px solid #d7163e;
    border-radius: 60px;
    background: #f63a61;
    cursor: pointer;

    & span {
      color: #fff;
      text-align: center;
      font-family: Apercu;
      font-size: 14px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
    }
  }
`;

function Header() {
  return (
    <StyledHeader>
      <StyledMenuWrapper>
        <StyledImg src={headerLogo} />
        <StyledMenu>
          <p>Product</p>
          <p>Pricing</p>
          <p>Company</p>
          <p>Resources</p>
          <p>Contact</p>
        </StyledMenu>
        <StyledContainer>
          <p>Login</p>
          <button>
            <span>Get Started</span>
          </button>
        </StyledContainer>
      </StyledMenuWrapper>
      <Divider />
    </StyledHeader>
  );
}

export default Header;
