import styled from "styled-components";
import SectionWrapper from "../components/SectionWrapper";

import ClickUpLogo from "../images/customers/ClickUp.svg";
import DropboxLogo from "../images/customers/Dropbox.svg";
import SegmentLogo from "../images/customers/Segment.svg";
import FreshbooksLogo from "../images/customers/Freshbooks.png";
import GitHubLogo from "../images/customers/GitHub.png";

const StyledCustomers = styled.div`
  display: inline-flex;
  width: 1022px;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  gap: 56px;
  flex-shrink: 0;
  background: #fff;

  & h1 {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 22px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  & div {
    display: flex;
    align-items: center;
    gap: 110px;
  }
`;

function Customers() {
  return (
    <SectionWrapper padding="120px 209px" justifyContent="center">
      <StyledCustomers>
        <h1>Trusted by 1000K plus customers</h1>
        <div>
          <img src={ClickUpLogo} alt="ClickUpLogo" />
          <img src={DropboxLogo} alt="DropboxLogo" />
          <img src={SegmentLogo} alt="SegmentLogo" />
          <img src={FreshbooksLogo} alt="FreshbooksLogo" />
          <img src={GitHubLogo} alt="GitHubLogo" />
        </div>
      </StyledCustomers>
    </SectionWrapper>
  );
}

export default Customers;
