import AboutUs from "./sections/AboutUs";
import CallToAction from "./sections/CallToAction";
import Customers from "./sections/Customers";
import Divider from "./sections/Divider";
import FAQ from "./sections/FAQ";
import Footer from "./sections/Footer";
import Header from "./sections/Header";
import HeroArea from "./sections/HeroArea";
import ProductScreenshot from "./sections/ProductScreenshot";
import Statistics from "./sections/Statistics";
import Team from "./sections/Team";

function App() {
  return (
    <>
      <Header />
      <HeroArea />
      <ProductScreenshot />
      <Divider />
      <Customers />
      <AboutUs />
      <Statistics />
      <CallToAction />
      <Team />
      <FAQ />
      <Footer />
    </>
  );
}

export default App;
