import styled from "styled-components";

import ArrowDown from "../images/faq/ArrowDown.svg";

const StyledFAQCard = styled.div`
  width: 480px;
  height: 70px;
  flex-shrink: 0;
  display: flex;
  flex: 45%;
  justify-content: space-between;
  align-items: center;
  padding: 24px 31px 24px 20px;
  border-radius: 5px;
  background: #fff;

  & p {
    color: #0f0049;
    font-family: Apercu;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
  }

  & img {
    width: 13.437px;
    height: 8.043px;
    flex-shrink: 0;
    cursor: pointer;
  }
`;

function FAQCard({ question }) {
  return (
    question && (
      <StyledFAQCard>
        <p>{question}</p>
        <img src={ArrowDown} alt="arrowDown" />
      </StyledFAQCard>
    )
  );
}

export default FAQCard;
