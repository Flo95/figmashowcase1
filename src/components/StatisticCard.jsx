import styled from "styled-components";

const StyledCardWrapper = styled.div`
  width: 280px;
  height: 287px;
  padding: 40px 10px 40px 40px;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  flex-shrink: 0;
  border-radius: 10px;
  background: #f6f5f8;

  & img {
    width: 70px;
    height: 70px;
    margin-bottom: 20px;
    flex-shrink: 0;
  }

  & h2 {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 40px;
    font-style: normal;
    font-weight: 700;
    line-height: normal;
    margin-bottom: 10px;
  }

  & p {
    color: #0f0049;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
  }
`;

function StatisticCard({ imgData, title, text }) {
  return (
    <StyledCardWrapper>
      <img src={imgData?.src} alt={imgData?.alt} />
      <h2>{title}</h2>
      <p>{text}</p>
    </StyledCardWrapper>
  );
}

export default StatisticCard;
