import styled from "styled-components";

const EmailWrapper = styled.div`
  width: 480px;
  height: 66px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  flex-shrink: 0;
  border-radius: 80px;
  background: #fff;
  margin-bottom: 20px;

  box-shadow: 0px 4px 4px 0px rgba(0, 0, 0, 0.05);

  & input {
    color: #111827;
    font-family: Apercu;
    font-size: 14px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    letter-spacing: -0.14px;
    opacity: 0.4;
    border: none;
    outline: none;
    margin-left: 20px;
    width: 60%;
  }

  & button {
    margin: 11px 10px 11px 0px;
    display: inline-flex;
    padding: 12px 15px;
    align-items: flex-start;
    gap: 10px;
    border-radius: 60px;
    border: 1px solid #d7163e;
    background: #f63a61;
    cursor: pointer;

    & span {
      color: #fff;
      text-align: center;
      font-family: Apercu;
      font-size: 16px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
    }
  }
`;

const StyledTextWrapper = styled.div`
  & span {
    margin-left: 7px;
    color: ${({ $hintColor }) => ($hintColor ? $hintColor : "#111827")};
    font-family: Apercu;
    font-size: 12px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    ${({ $hintOpacity }) => $hintOpacity && "opacity: 0.4;"}
  }
`;

function EmailInput({ imgData, hintColor, hintOpacity = true }) {
  return (
    <>
      <EmailWrapper>
        <input type="email" placeholder="Enter your work email" />
        <button>
          <span>Get Started</span>
        </button>
      </EmailWrapper>
      <StyledTextWrapper $hintColor={hintColor} $hintOpacity={hintOpacity}>
        <img src={imgData?.src} alt={imgData?.alt} />
        <span>No credit card required</span>
      </StyledTextWrapper>
    </>
  );
}

export default EmailInput;
