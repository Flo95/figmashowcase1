import styled from "styled-components";

const StyledTeamCard = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;

  & img {
    width: 280px;
    height: 280px;
    flex-shrink: 0;
    border-radius: 10px;
    background: url(<path-to-image>) lightgray 50% / cover no-repeat;
  }

  & h6 {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 18px;
    font-style: normal;
    font-weight: 500;
    line-height: normal;
    margin-top: 30px;
    margin-bottom: 5px;
  }

  & p {
    color: #0f0049;
    text-align: center;
    font-family: Apercu;
    font-size: 16px;
    font-style: normal;
    font-weight: 400;
    line-height: normal;
    opacity: 0.5;
  }
`;

function TeamCard({ imgData, name, designation = "Designation Goes here" }) {
  return (
    imgData &&
    name && (
      <StyledTeamCard>
        <img src={imgData?.src} alt={imgData?.alt} />
        <h6>{name}</h6>
        <p>{designation}</p>
      </StyledTeamCard>
    )
  );
}

export default TeamCard;
