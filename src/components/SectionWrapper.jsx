import styled from "styled-components";

const StyledSectionWrapper = styled.section`
  padding: ${({ $padding }) => ($padding ? $padding : "0px")};
  display: flex;
  flex-direction: ${({ $direction }) => ($direction ? $direction : "row")};
  justify-content: ${({ $justifyContent }) =>
    $justifyContent ? $justifyContent : "flex-start"};
  align-items: ${({ $alignItems }) => ($alignItems ? $alignItems : "normal")};
  background: ${({ $backgroundColor }) =>
    $backgroundColor ? $backgroundColor : "#fff"};
`;

function SectionWrapper({
  children,
  padding,
  direction,
  justifyContent,
  alignItems,
  backgroundColor,
}) {
  return (
    <StyledSectionWrapper
      $padding={padding}
      $direction={direction}
      $justifyContent={justifyContent}
      $alignItems={alignItems}
      $backgroundColor={backgroundColor}
    >
      {children}
    </StyledSectionWrapper>
  );
}

export default SectionWrapper;
