# Figma Showcase 1

Umsetzung eines Figma-Designs von der Figma Community.

[Figma Design ->](<https://www.figma.com/design/qZqSzAWvuTqkfWhfBBzqio/%F0%9F%96%A5%EF%B8%8F--Free-Website-Template-(Community)?node-id=1-6&m=dev>)

[LiveDemo ->](https://figma-showcase1.netlify.app/)

## Verwendete Technologien

- React.js
- Styled Components
